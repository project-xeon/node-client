global.game = {};
let Terminal = require('./modules/terminal');
let cwd = '/';
let address = 'localhost';
Object.defineProperties(game, {
    'prompt': {
        get: function(){
            return `¬Groot@${address}¬W:¬B${cwd == '/' ? '~' : cwd}¬W# `;
        }
    },
    'address': {
        get: function(){
            return address;
        },
        set: function(v){
            address = v;
        }
    },
    'cwd': {
        get: function(){
            return cwd;
        },
        set: function(v){
            cwd = v;
        }
    }
});
let WebSocket = require('./modules/ws');
let RPC = require('./modules/rpc');
const CommandHandler = require('./modules/commands');
let ws;

async function loop(){
    let command = await Terminal.input.text(game.prompt);
    try {
        await CommandHandler.run.call(ws, command);
    } catch(e) {
        await Terminal.print(`¬RError: ¬r${e.message}`);
    }
    loop();
}

async function main(){
    Terminal.print('Establishing WebSocket connection...');
    ws = await WebSocket.connect('wss://game.project-xeon.net/ws');
    Terminal.print('Connection established.');
    Terminal.print('Connecting to Discord...');
    let code = await RPC.connect();
    Terminal.print('Discord connection established.');
    ws.once('access', async (token)=>{
        await RPC.login(token);
        ws.send('ping');
        CommandHandler.load();
        Terminal.clear();
        Terminal.print('¬g[¬GConnection to XEON Network Successful¬g]')
        loop();
    });
    ws.on('print', Terminal.print);
    ws.send('auth', {code: code});
}

Terminal.clear();

main();