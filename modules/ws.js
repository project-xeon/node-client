const WebSocket = require('ws');
var Terminal = require('./terminal');
const fs = require('fs');
const path = require('path');
let inbound = [];
let outbound = [];
let noop = ()=>{};
if(!fs.existsSync(path.join(process.cwd(), 'logs'))) fs.mkdirSync(path.join(process.cwd(), 'logs'));
async function logInbound(line){
    inbound.push(line);
    fs.writeFile(path.join(process.cwd(), 'logs', 'inbound.log'), inbound.join('\n'), noop);
}
async function logOutbound(line){
    outbound.push(line);
    fs.writeFile(path.join(process.cwd(), 'logs', 'outbound.log'), outbound.join('\n'), noop);
}
const util = require('./util');
module.exports.connect = function(uri){
    return new Promise((resolve, reject)=>{
        let ws = new WebSocket(uri);
        ws.once('open', ()=>{
            ws._send = ws.send;
            ws.send = function(event, data){
                if(ws.readyState == WebSocket.OPEN) {
                    let d = JSON.stringify({op: event, ts: Date.now(), data: data});
                    ws._send(d);
                    logOutbound(d);
                    return true;
                } else return false;
            }
            resolve(ws);
        });
        ws.once('error', (err)=>{
            reject(err);
        });
        ws.on('json', (dobj)=>{
            if(dobj.op == 'pong') {
                setTimeout(()=>{
                    ws.send('ping');
                }, 20000);
            }
        });
        ws.on('message', (data)=>{
            logInbound(data)
            if(util.validateJSON(data)){
                try {
                    data = JSON.parse(data);
                    ws.emit('json', data);
                    if(data.op && data.data){
                        ws.emit(data.op, data.data);
                    }
                } catch(e) {
                    ws.emit('badJSON', data);
                }
            }
        });
        ws.on('close', ()=>{
            Terminal.print('¬RConnection closed.');
        });
    });
}