const RPC = require('discord-rpc');

let client;

module.exports.connect = function(){
    return new Promise(async (resolve, reject)=>{
        client = new RPC.Client({transport: 'ipc'});
        client.once('error', (err)=>{
            client = null;
            reject(err);
        });
        await client.connect('511590542627569694');
        let data = await client.request('AUTHORIZE', {client_id: '511590542627569694', scopes: ['rpc', 'identify']});
        resolve(data.code);
    });
}

module.exports.login = function(token){
    return new Promise(async (resolve, reject)=>{
        if(client){
            client.once('ready', ()=>{
                client.setActivity({
                    state: 'Connected',
                    largeImageKey: 'icon',
                    startTimestamp: new Date()
                });
                resolve();
            });
            client.once('error', (err)=>{
                client = null;
                reject(err);
            });
            await client.login({clientId: '511590542627569694', accessToken: token, scopes: ['rpc', 'identify']});
            delete token;
        } else {
            reject('Client does not exist.');
        }
    });
}