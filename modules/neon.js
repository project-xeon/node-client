let {Neon} = require('ne');
let Terminal = require('./terminal');
let util = require('./util');
const EventEmitter = require('events');

let events = new EventEmitter();

let noop = function(){};

Neon.prototype.save = function save( callback ) {
	if ( typeof callback !== 'function' ) { callback = noop ; }
    this.pauseMainUserEvent = true ;
    callback();
    // TODO: Add eval code to send back to server.
    this.terminate(this.textBuffer.getText());
};

Neon.prototype.terminate = function terminate(text) {
    setTimeout( () => {
        this.screenBuffer.clear();
        this.term.clear();
        Terminal.eraseDisplay();
        Terminal.clear();
        events.emit(this.uuid, text);
    } , 100 ) ;
};

Neon.prototype.run = function run(path, data) {
	if ( this.standAlone ) { this.standAloneInit() ; }

    this.statusBar( path ) ;
    this.textBuffer.setText(data);
    this.textBuffer.setEmptyCellAttr( this.emptyCellAttr ) ;
	this.term.grabInput() ;
	// Finish init
	this.draw() ;
	// Bind the 'key' event to the key handler
	this.term.on( 'key' , Neon.prototype.onKey.bind( this ) ) ;

	// Bind the 'terminal' event to the terminal event handler
	this.term.on( 'terminal' , Neon.prototype.onTerminal.bind( this ) ) ;
} ;

Neon.prototype.statusBar = function statusBar( str ) {
	return;
} ;



Neon.prototype.contextBar = function contextBar( str , options ) {
	return;
} ;

module.exports.openEditor = function(path, data){
    return new Promise((resolve, reject)=>{
        let editor = new Neon({hasStatusBar: false, hasContextBar: false});
        editor.uuid = util.generateUUID();
        editor.run(path, data);
        events.once(editor.uuid, (text)=>{
            editor = null;
            resolve(text);
        });
    });
}