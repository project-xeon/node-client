let commands = new Map();
let Editor = require('./neon');
let Terminal = require('./terminal');
let Eval = require('./eval');
const util = require('./util');
const fs = require('fs');
const Path = require('path');

module.exports.run = function(command){
    let ws = this;
    return new Promise((resolve, reject)=>{
        var cmd = command.split(' ')[0];
        var args = command.split(' ').slice(1);
        if(commands.get(cmd)){
            try {
                commands.get(cmd)({resolve: resolve, reject: reject, ws: ws}, args);
            } catch(e) {
                Terminal.print(`¬RError: ¬r${e.message}`);
            }
        } else if(cmd == '.reload') {
            module.exports.load();
            Terminal.print('Commands reloaded.');
            resolve();
        } else {
            reject(new Error('Unknown command.'));
        }
    });
}

module.exports.load = function(){
    let dir = Path.join(process.cwd(), 'modules', 'commands');
    fs.readdirSync(dir).forEach(filename=>{
        let file = Path.join(dir, filename);
        let data = fs.readFileSync(file).toString();
        eval(data);
    });
}