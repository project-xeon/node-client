const chalk = require('chalk');

var colors = {
    'w':'#FFFFFF',
    'W':'#CCCCCC',
    'R':'#FF0000',
    'r':'#FF8787',
    'G':'#00FF00',
    'g':'#87FF87',
    'B':'#2666FF',
    'b':'#00FFFF',
    'y':'#FFFF00',
    'o':'#FF8700',
    'P':'#FF00FF',
    'p':'#FF87FF',
    'V':'#8A29E0',
    'v':'#635294',
    '*':'#FFFFFF',
    '1':'#2bff26',
    '2':'#2bd1f2',
    '3':'#bf52ff',
    '4':'#faff33',
    '?':'#E83D78'
};

module.exports = function(text){
    var f = '';
    var t = '';
    var color = '*';
    text = text.split('');
    var skip = -1;
    text.forEach((ch, i)=>{
        if(ch == '¬'){
            var c = text[i+1];
            if(colors[c] != undefined){
                f += chalk.hex(colors[color])(t);
                t = '';
                color = c;
                skip = 0;
            }
        }
        if(skip == -1){
            t += ch;
        } else if(skip == 0){
            skip = 1;
        } else if(skip == 1){
            skip = -1;
        }
    });
    if(t.length > 0) f += chalk.hex(colors[color])(t);
    return f;
}