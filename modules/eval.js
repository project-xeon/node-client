const util = require('./util');
module.exports = function(code){
    return new Promise((resolve, reject)=>{
        var success = this.send('eval', code);
        if(success) {
            this.once('eval', (res)=>{
                if(util.validateJSON(res)){
                    try {
                        res = JSON.parse(res);
                    } catch(e) {

                    }
                }
                resolve(res);
            });
        } else {
            reject(new Error('Could not send to server.'));
        }
    });
}