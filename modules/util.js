const Random = require('random-js');
const random = new Random(Random.engines.browserCrypto);
const Path = require('path');

module.exports.parseArguments = function(args, options = []){
    var t = [];
    var buffer = [];
    args.forEach(arg=>{
        if((arg[0] == "'" || arg[0] == '"') && !(arg[arg.length-1] == '"' || arg[arg.length-1] == "'") && buffer.length == 0) {
            buffer.push(arg.substring(1));
        } else if(!(arg[0] == "'" || arg[0] == '"') && (arg[arg.length-1] == '"' || arg[arg.length-1] == "'") && buffer.length != 0){
            buffer.push(arg.substring(0, arg.length-1));
            t.push(buffer.join(' '));
            buffer = [];
        } else if((arg[0] == "'" || arg[0] == '"') && (arg[arg.length-1] == '"' || arg[arg.length-1] == "'")) {
            t.push(arg.substring(1, arg.length-1));
        } else if(buffer.length != 0){
            buffer.push(arg);
        } else {
            t.push(arg);
        }
    });
    args = null;
    var opts = {};
    if(options instanceof Array){
        options.forEach(opt=>{
			var i = -1;
			t.some((el, i2)=>{
				if(opt.alias ? (el == `-${opt.alias}` || el == `--${opt.name}` || el.substring(0, opt.alias.length+1) == `-${opt.alias}` || el.substring(0, opt.name.length+2) == `--${opt.name}`) : (el == `--${opt.name}` || el.substring(0, opt.name.length+2) == `--${opt.name}`)) {
					i = i2;
					return true;
				}
			});
            if(i != -1){
                if(opt.type == String){
                    var e = t[i];
                    if(e.split('=').length == 2){
                        t.splice(i, 1);
                        e = e.split('=')[1];
                    } else {
                        e = t[i+1];
                        t.splice(i, 2);
                    }
                    opts[opt.name] = e;
                } else if (opt.type == Boolean) {
                    opts[opt.name] = true;
                }
            } else {
                if(opt.type == String){
                    opts[opt.name] = null;
                } else if(opt.type == Boolean){
                    opts[opt.name] = false;
                }
            }
        });
    }
    opts._args = t;
    return opts;
}

module.exports.validateJSON = function(data){
    var rx = new RegExp(/^\{[\s\S]*\}|\[[\s\S]*\]/g);
    if(typeof data != 'string') return false;
    return rx.test(data);
}

module.exports.generateUUID = function(){
    return random.uuid4();
}

module.exports.resolvePath = function(path){
    var rx = new RegExp(/\~/g);
    path = path.replace(rx, '/');
    if(!Path.isAbsolute(path)){
        path = Path.join(game.cwd, path);
    }
    var rx1 = new RegExp(/\\/g);
    var rx2 = new RegExp(/\/{2, }/g);
    return path.replace(rx1, '/').replace(rx2, '/');
}