var term = require('terminal-kit').terminal;
const colorize = require('./colorize');
const fs = require('fs');
const path = require('path');
let history = [];

function addHistory(command){
    history.push(command);
    fs.writeFile(path.join(process.cwd(), 'history.txt'), history.join('\n'), (err)=>{
        if(err) throw err;
    });
}

function loadHistory(){
    try {
        let hist = fs.readFileSync(path.join(process.cwd(), 'history.txt')).toString().split('\n');
        history = hist;
    } catch(e) {
        history = [];
        fs.writeFileSync(path.join(process.cwd(), 'history.txt'), '');
    }
}

loadHistory();

let current_input = null;
let current_prompt = '';

function inputField(prompt, opts){
    return new Promise((resolve, reject)=>{
        current_prompt = prompt;
        term(colorize(prompt));
        current_input = term.inputField(opts, (err, input)=>{
            if(opts.echo != false) addHistory(input);
            current_input = null;
            term('\n\r');
            resolve(input);
        });
    });
}

term.on('key', (name)=>{
    if(name == 'CTRL_C'){
        term.clear();
        term.processExit();
    }
});

module.exports.print = async function(text){
    if(!text || text == '') {
        return false;
    } else {
        if(current_input != null){
            current_input.hide();
        }
        term('\r');
        console.log(colorize(text));
        if(current_input != null){
            term(colorize(current_prompt));
            let pos = await term.getCursorLocation();
            current_input.rebase(pos.x, pos.y);
            current_input.show();
        }
    }
}

module.exports.eraseDisplay = term.eraseDisplay;

module.exports.test = function(){
    current_input.hide();
    setTimeout(()=>{
        current_input.show();
    }, 5000);
}

module.exports.input = {
    text: function(prompt){
        return inputField(prompt, {
            minLength: 1,
            history: history
        });
    },
    password: function(prompt){
        return inputField(prompt, {
            minLength: 1,
            echo: false
        });
    }
}

module.exports.clear = term.clear;