commands.set('rm', async (command, args)=>{
    var i = args.findIndex(el=>(el == '-r' || el == '--recursive'));
    if(i != -1) {
        args.splice(i, 1);
        i = true;
    } else {
        i = false;
    }
    if(!args[0]) {
        Terminal.print('Path required.');
        command.resolve();
    } else {
        let path = util.resolvePath(args[0]);
        var com = await Eval.call(command.ws, `(async (path, recurse)=>{
            try {
                await drives.rm(path, recurse);
                return {success: true};
            } catch(e) {
                return {success: false, error: e.message};
            }
        })("${path}", ${i})`);
        if(com.success == true){
            command.resolve();
        } else {
            Terminal.print(com.error);
            command.resolve();
        }
    }
});