commands.set(':', async (command, args)=>{
    let res = await Eval.call(command.ws, args.join(' '));
    switch(typeof res){
        case 'object':
            res = JSON.stringify(res, null, '   ');
        break;
        case 'function':
            res = '[function]';
        break;
    }
    Terminal.print(`${res}`);
    command.resolve();
});