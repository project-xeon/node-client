commands.set('disconnect', async (command, args)=>{
    let result = await Eval.call(command.ws, `disconnect`);
    if(result.success){
        Terminal.print(result.msg);
        game.address = result.address;
        command.resolve();
    } else {
        Terminal.print(result.error);
        command.resolve();
    }
});