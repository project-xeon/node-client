commands.set('nano', async (command, args)=>{
    let path = util.resolvePath(args[0]);
    let ret = await Eval.call(command.ws, `(async ()=>{
        try {
            let exists = await drives.exists("${path}");
            if(exists){
                let data = await drives.read("${path}");
                return {success: true, data: data.toString()};
            } else {
                return {success: true, data: ""};
            }
        } catch(e) {
            return {success: false, error: e.message};
        }
    })()`);
    if(ret.success){
        var buffer = await Editor.openEditor(path, ret.data);
        let res = await Eval.call(command.ws, `(async ()=>{
            try{
                await drives.write("${path}", ${JSON.stringify(Buffer.from(buffer).toJSON().data)});
                return {success: true};
            } catch(e) {
                return {success: false, error: e.message};
            }
        })()`);
        if(res.success) {
            Terminal.print('File saved.');
        } else {
            Terminal.print(res.error);
        }
        command.resolve();
    } else {
        Terminal.print(`¬R${ret.error}`);
        command.resolve();
    }
});