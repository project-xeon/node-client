commands.set('ls', async (command, args)=>{
    let path = util.resolvePath(args[0] || './');
    let files = await Eval.call(command.ws, `(async ()=>{
        let dir = await drives.readdir("${path}");
        let drive = await drives.get();
        let data = [];
        let path = "${path}";
        dir.forEach(d=>{
            let p = path+d;
            data.push({filename: d, directory: (drive[p] == null)});
        });
        return data;
    })()`);
    if(typeof files == 'object' && files != null) {
        Terminal.print(files.map(file=>{
            return file.directory ? `¬B${file.filename}¬*` : `¬g${file.filename}`;
        }).join('  '));
        command.resolve();
    } else {
        command.resolve();
    }
});