commands.set('connect', async (command, args)=>{
    args = util.parseArguments(args)._args;
    let result = await Eval.call(command.ws, `connect ${args[0]} ${args[1]}`);
    if(result.success){
        Terminal.print(result.msg);
        game.address = result.address;
        command.resolve();
    } else {
        Terminal.print(result.error);
        command.resolve();
    }
});