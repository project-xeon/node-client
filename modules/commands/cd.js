commands.set('cd', async (command, args)=>{
    let path = util.resolvePath(args[0] || './');
    if(path == game.cwd) command.resolve();
    else {
        let valid = await Eval.call(command.ws, `(async (path)=>{
            let exists = await drives.exists(path);
            if(exists){
                try {
                    let d = await drives.read(path);
                    return {success: false, error: 'Path is not a directory.'};
                } catch(e) {
                    return {success: true};
                }
            } else {
                return {success: false, error: 'Path does not exist.'};
            }
        })("${path}")`);
        if(valid.success){
            game.cwd = path;
            command.resolve();
        } else {
            Terminal.print(valid.error);
            command.resolve();
        }
    }
});